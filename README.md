![](https://img.shields.io/badge/Microverse-blueviolet)

# Todo-list-webpack

> This website Todo-list-webpack provides users a convenient way to keep track of their todos.

![screenshot](./screen.gif)

## Built With

- HTML
- CSS
- JavaScript
- Linters

## Live Demo
[Todos-Live](https://omarsalem7.github.io/Todo-list-webpack/)
## Instructions to run locally:
### To run locally you should have npm and node in your machine
1. Clone the project using git.
2. write npm install in your terminal to install dependencies
3. write npm start to see the output.

## Authors

👤 **Omar Salem**

- GitHub: [Omar Salem](https://github.com/omarsalem7)
- Twitter: [Omar Salem](https://twitter.com/Omar80491499)
- LinkedIn: [Omar Salem](https://www.linkedin.com/in/omar-salem-a6945b177/)


## 🤝 Contributing

Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](../../issues/).

## Show your support

Give a ⭐ if you like this project!

## 📝 License

This project is [Minimalist](https://web.archive.org/web/20180320194056/http://www.getminimalist.com:80/) licensed.
